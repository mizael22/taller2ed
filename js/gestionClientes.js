var clientesObtenidos;
function getClientes() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
    //console.table(JSON.parse(request.responseText).value);
    clientesObtenidos = request.responseText;
    procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();
}
function procesarClientes() {
  var jsonClientes= JSON.parse(clientesObtenidos);
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  //alert(jsonClientes.value[0].ProductName);
  var divClientes = document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

   tabla.classList.add("table");
   tabla.classList.add("table-striped");

  for (var i = 0; i < jsonClientes.value.length; i++) {
    //console.log(jsonClientes.value[i].ProductName);
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = jsonClientes.value[i].ContactName;

    var columnaCity = document.createElement("td");
    columnaCity.innerText = jsonClientes.value[i].City;

    var columnaCountry = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");

    if(jsonClientes.value[i].Country !="UK"){
      imgBandera.src = rutaBandera + jsonClientes.value[i].Country + ".png";
    }else{
      imgBandera.src = rutaBandera + "United-Kingdom" + ".png";
    }
    columnaCountry.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(columnaCountry);

    tbody.appendChild(nuevaFila);


  }
  tabla.appendChild(tbody);
  divClientes.appendChild(tabla);

}
